# cki-lib

Common place to put scripts used across CKI infrastructure.

## Maintainers

* [jracek](https://gitlab.com/jracek)
* [inakimalerba](https://gitlab.com/inakimalerba)

## Design principles

This repository hold code that is used within other CKI Project applications,
including:

* autowaive
* beaker-metrics
* bkr_respin
* data warehouse
* pipeline-definition
* pipeline-stats
* pipeline-trigger
* misc-tools
* reporter-ng
* skt
* umb messenger
* upt

## Overriding cki-lib in pipeline-definition

The used cki-lib version can be overriden using `cki_lib_targz_url` trigger
variable.

## Optional dependencies

To use the `teiid` module, use the `teiid` extra like

```shell
python3 -m pip install git+https://gitlab.com/cki-project/cki-lib.git/#egg=cki_lib[teiid]
```

## Container entrypoint script `cki_entrypoint.sh`

The entrypoint script provides a best-practice container entrypoint script for
CKI Python container images. It is configured via environment variables:

| Variable            | Description                                                                             |
|---------------------|-----------------------------------------------------------------------------------------|
| `START_REDIS`       | Start a Redis server                                                                    |
| `START_CELERY_xxxx` | Start Celery workers with the given command line arguments, one worker per variable     |
| `START_FLASK`       | Start the given Flask app via gunicorn (`IS_PRODUCTION=true`) or the Flask devel server |
| `START_PYTHON_xxxx` | Start Python modules with the given command line arguments, one module per variable     |
| `LOG_NAME`          | Append all stdout/stderr output to `/logs/$LOG_NAME.log`                                |

The following example starts a Redis server, two celery workers and a Flask app:

```shell
#!/bin/bash
export START_REDIS=true
export START_CELERY_MATCHER="--app pipeline_herder.worker --concurrency 1 --queues matching"
export START_CELERY_HERDER="--app pipeline_herder.worker --concurrency 1 --queues herding"
export START_FLASK=pipeline_herder.webhook
export LOG_NAME=pipeline-herder
exec cki_entrypoint.sh
```

**It is important that the entrypoint script is started via exec! Otherwise, the process tree cannot be killed on failures!**

## Developer guidelines

**Use care when making changes to this repository.** Many other applications
depend on this code to work properly.

### Setup

If you want to get started with development, you can install the
dependencies by running:

```shell
pip install --user .[dev]
```

### Tests + linting

To run the tests (together with linting), execute the following command:

```shell
tox
```
