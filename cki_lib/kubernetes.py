"""Kubernetes Helper."""
import os
import pathlib

import kubernetes


class KubernetesHelper:
    """Provide some Kubernetes helper functions."""

    secrets_path = '/var/run/secrets/kubernetes.io/serviceaccount'

    def __init__(self):
        """Create a new Kubernetes helper."""
        self.config = kubernetes.client.Configuration()
        self.config_type = None
        self.namespace = None

    def setup(self):
        """Set up the configuration to allow API calls."""
        if 'OPENSHIFT_KEY' in os.environ:
            self._setup_environment()
        elif os.path.isdir(self.secrets_path):
            self._setup_cluster()
        else:
            self._setup_kube_config()

    @property
    def api_client(self):
        """Return ApiClient."""
        return kubernetes.client.ApiClient(configuration=self.config)

    def api_corev1(self):
        """Return a Kubernetes CoreV1Api client."""
        return kubernetes.client.CoreV1Api(self.api_client)

    def api_batchv1beta1(self):
        """Return a Kubernetes BatchV1beta1Api client."""
        return kubernetes.client.BatchV1beta1Api(self.api_client)

    def _setup_environment(self):
        self.config_type = 'OPENSHIFT_* environment variables'
        self._update_config(os.environ['OPENSHIFT_SERVER'],
                            os.environ['REQUESTS_CA_BUNDLE'],
                            os.environ['OPENSHIFT_KEY'])
        self.namespace = os.environ['OPENSHIFT_PROJECT']

    def _setup_cluster(self):
        self.config_type = 'cluster configuration'
        self._update_config((f'https://{os.environ["KUBERNETES_SERVICE_HOST"]}'
                             f':{os.environ["KUBERNETES_SERVICE_PORT"]}'),
                            os.path.join(self.secrets_path, 'ca.crt'),
                            self._get_secret('token'))
        self.namespace = self._get_secret('namespace')

    def _setup_kube_config(self):
        self.config_type = '~/.kube/config'
        kubernetes.config.load_kube_config(client_configuration=self.config)
        config_contexts = kubernetes.config.list_kube_config_contexts()
        self.config.ssl_ca_cert = os.environ['REQUESTS_CA_BUNDLE']
        self.namespace = config_contexts[1]['context']['namespace']

    def _update_config(self, server, ca_crt, key):
        self.config.api_key_prefix['authorization'] = 'Bearer'
        self.config.api_key['authorization'] = key
        self.config.host = server
        self.config.ssl_ca_cert = ca_crt

    def _get_secret(self, name):
        return pathlib.Path(self.secrets_path, name).read_text('utf-8')
