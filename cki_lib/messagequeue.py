"""RabbitMQ Message Queue helper."""
import contextlib
import functools
import json
import os
import queue
import ssl
import threading
import uuid

import pika

from . import logger
from . import misc
from . import timer

LOGGER = logger.get_logger(__name__)


class MessageQueue:
    """
    RabbitMQ message queue helper.

    Helper to handle queue initialization and message sending.
    MessageQueue.connect() should be used to get a context manager for a
    `pika.channel.Channel`.

    host: RabbitMQ server address. Defaults to the value of the RABBITMQ_HOST
        environment variable or localhost.
    port: RabbitMQ server port. Defaults to the value of the RABBITMQ_POST
        environment variable or 5672. With a port of 443 or 5671, uses SSL.
    user: RabbitMQ server user. Defaults to the value of the RABBITMQ_USER
        environment variable or guest.
    password: RabbitMQ server password. Defaults to the value of the
        RABBITMQ_PASS environment variable or guest.
    cafile: ca certificates
    certfile: SSL client private key and corresponding certificate.
    keepalive_s: seconds to keep the channel open after sending the message.
    """

    def __init__(self, host=None, port=None, user=None, password=None,
                 connection_params=None, cafile=None, certfile=None,
                 keepalive_s=0):
        # pylint: disable=too-many-arguments
        """Init."""
        host = host or os.environ.get('RABBITMQ_HOST', 'localhost')
        port = int(port or misc.get_env_int('RABBITMQ_PORT', 5672))
        user = user or os.environ.get('RABBITMQ_USER', 'guest')
        password = password or os.environ.get('RABBITMQ_PASSWORD', 'guest')
        connection_params = dict(connection_params or {})
        if (port in (443, 5671) or certfile or cafile) and \
                'ssl_options' not in connection_params:
            connection_params['ssl_options'] = pika.SSLOptions(
                ssl.create_default_context(cafile=cafile))
        if certfile:
            self.credentials = pika.credentials.ExternalCredentials()
            connection_params['ssl_options'].context.load_cert_chain(certfile)
        else:
            self.credentials = pika.PlainCredentials(user, password)
        self.connection_params = pika.ConnectionParameters(
            host=host.rstrip('/'), port=port, credentials=self.credentials,
            **connection_params)

        self.keepalive_s = keepalive_s
        self._channel_lock = threading.Lock()
        self._channel = None
        self._disconnect_timer = timer.ScheduledTask(
            keepalive_s, self._disconnect
        )

    def queue_init(self, name, params=None):
        """Create queue on remote server."""
        with self.connect() as channel:
            channel.queue_declare(name, **(params or {}))

    def send_message(self, data, queue_name, exchange=''):
        """
        Send message to queue.

        Encode `data` as json and send it to routing_key=queue_name,
        exchange=exchange.
        """
        self.send_messages_list([(data, queue_name, exchange)])

    def send_messages_list(self, messages):
        """
        Send a list of messages to queue.

        Like send_message but for bulk sending.

        Parameters:
        messages: list of messages with format: (data, queue_name, exchange).
        """
        with self.connect() as channel:
            for data, queue_name, exchange in messages:
                body = json.dumps(data)
                channel.basic_publish(
                    body=body, exchange=exchange, routing_key=queue_name
                )

    # pylint: disable=too-many-arguments
    def _consume_messages_thread(self, thread_queue, thread_quit,
                                 thread_dead_channel,
                                 exchange: str, routing_keys: list,
                                 queue_name: str = None,
                                 prefetch_count: int = 5,
                                 inactivity_timeout: int = None,
                                 return_on_timeout: bool = True):

        try:
            with self.connect() as channel:
                if prefetch_count:
                    channel.basic_qos(prefetch_count=prefetch_count)
                if misc.is_production() and queue_name:
                    # production queue, as durable as possible to not lose msgs
                    channel.queue_declare(queue_name, durable=True)
                else:
                    # temporary queue, uuid format for fedora-messaging
                    queue_name = str(uuid.uuid4())
                    channel.queue_declare(queue_name, auto_delete=True)

                # We're expecting routing_keys to be a list, but str is also ok
                if isinstance(routing_keys, str):
                    routing_keys = [routing_keys]

                for routing_key in routing_keys:
                    channel.queue_bind(queue_name, exchange,
                                       routing_key=routing_key)
                for method, _, body in channel.consume(
                        queue_name, inactivity_timeout=inactivity_timeout):
                    if thread_quit.is_set():
                        return
                    if not method:  # inactivity timeout
                        if return_on_timeout:
                            return
                        thread_queue.put((channel, None, None, 'null'))
                        continue

                    LOGGER.info('Received payload from %s (%s)',
                                method.routing_key, method.delivery_tag)
                    thread_queue.put((channel, method.delivery_tag,
                                      method.routing_key, body))
        # pylint: disable=broad-except
        except Exception:
            # an exception most likely means that the channel is dead
            thread_dead_channel.set()
            LOGGER.exception('Exception in consume messages thread')
        finally:
            thread_queue.put(None)  # terminate consumer

    @staticmethod
    def _consume_one(item, callback, manual_ack):
        try:
            channel, delivery_tag, routing_key, body = item
            LOGGER.debug('Processing payload from %s (%s)',
                         routing_key, delivery_tag)
            basic_ack_fn = functools.partial(
                channel.basic_ack, delivery_tag)
            add_callback_fn = functools.partial(
                channel.connection.add_callback_threadsafe, basic_ack_fn)
            try:
                if manual_ack:
                    callback(routing_key, json.loads(body),
                             add_callback_fn if delivery_tag else None)
                else:
                    callback(routing_key, json.loads(body))
            finally:
                LOGGER.debug('Processed payload from %s (%s)',
                             routing_key, delivery_tag)
            if not manual_ack and delivery_tag:
                add_callback_fn()
        # pylint: disable=broad-except
        except Exception:
            LOGGER.exception('Message handling failure, '
                             'will be requeued after restart')

    def consume_messages(self, exchange: str, routing_keys: list,
                         callback: callable, *args,
                         manual_ack=False, **kwargs):
        """Endlessly consume messages.

        If manual_ack=False, the callback has the signature (routing_key,
        body). If manual_ack=True, the callback has the signature (routing_key,
        body, ack_fn) and is expected to call ack_fn when it wants to
        acknowledge a message.

        If return_on_timeout=False, the callback will be called with
        routing_key=None, body=None and ack_fn=None on timeout.
        """
        thread_queue = queue.SimpleQueue()
        thread_quit = threading.Event()
        thread_dead_channel = threading.Event()
        threading.Thread(target=lambda: self._consume_messages_thread(
            thread_queue, thread_quit, thread_dead_channel,
            exchange, routing_keys, *args, **kwargs), daemon=True).start()
        try:
            while not thread_dead_channel.is_set():
                item = thread_queue.get()
                if not item:
                    break
                self._consume_one(item, callback, manual_ack)
        finally:
            thread_quit.set()  # terminate producer on signal in queue.get()

    @contextlib.contextmanager
    def connect(self):
        """Connect to the server and return a channel."""
        if self.keepalive_s:
            return self._connect_and_keepalive()
        return self._connect_no_keepalive()

    def _connect_no_keepalive(self):
        """Create connection and close it after use."""
        connection = pika.BlockingConnection(self.connection_params)
        LOGGER.info('Creating new channel')
        try:
            yield connection.channel()
        finally:
            connection.close()

    def _connect_and_keepalive(self):
        """Create connection and schedule timer to close it."""
        self._disconnect_timer.cancel()
        with self._channel_lock:
            if not self._channel:
                connection = pika.BlockingConnection(
                    self.connection_params
                )
                LOGGER.info('Creating new channel')
                self._channel = connection.channel()
            try:
                yield self._channel
            finally:
                self._disconnect_timer.start()

    def _disconnect(self):
        """Close the connection."""
        LOGGER.debug('Closing the connection')
        with self._channel_lock:
            if self._channel:
                self._channel.connection.close()
                self._channel = None
