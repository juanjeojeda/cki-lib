"""Misc utility functions."""
import collections
import contextlib
import distutils.util
import fcntl
import logging
import mailbox
import os
import pathlib
import subprocess
import tempfile
from configparser import ConfigParser
from contextlib import contextmanager
from email.header import decode_header

from .logger import get_logger
from .retrying import retrying_on_exception
from .session import get_session

LOGGER = get_logger(__name__)
SESSION = get_session(__name__)


class EnvVarNotSetError(Exception):
    """Requested environment variable is not set."""


@contextlib.contextmanager
def only_log_exceptions(exceptions=Exception):
    """Log, but ignore exceptions."""
    try:
        yield
    except exceptions:  # pylint: disable=broad-except
        LOGGER.exception('Ignored by context manager')


def shorten_url(url):
    """Attempt to shorten a URL."""
    with only_log_exceptions():
        shortener_url = os.environ.get('SHORTENER_URL')
        if shortener_url:
            response = SESSION.post(shortener_url, data={'url': url})
            response.raise_for_status()
            url = response.text.strip()
    return url


def get_nested_key(data, key, default=None):
    """
    Dig through nested dictionaries to get the value of a key.

    Inputs a key slash-separated like key_a/key_b/key_c, and returns 'value' on
    the following chain: {'key_a': {'key_b': {'key_c': value}}} handling
    missing keys.

    The delimiter is / to play well with dot-prefix "internal" keys used in
    quite some places.
    """
    subkeys = key.split('/')
    for subkey in subkeys:
        try:
            data = data[subkey]
        except KeyError:
            return default

    return data


def normalize_subject(subject):
    """Decode subject of an email, regardless of encoding."""
    result = ''
    for text, encoding in decode_header(subject):
        if not encoding:
            if isinstance(text, bytes):
                result += text.decode('utf-8')
            else:
                result += text
        else:
            result += text.decode(encoding)

    return result


def get_env_var_or_raise(env_key):
    """Retrieve the value of an environment variable or raise exception.

    Args:
        env_key:      Name of the variable's value to retrieve.

    Returns:
        Value of the environment variable, if set.

    Raises:
        EnvVarNotSetError if the variable is not set.

    """
    env_var = os.getenv(env_key)
    if not env_var:
        raise EnvVarNotSetError(f'Environment variable {env_key} is not set!')
    return env_var


def booltostr(value):
    """Convert a boolean to True/False strings."""
    return str(bool(value))


def strtobool(value):
    """Convert True/False strings to bool."""
    return bool(distutils.util.strtobool(value))


def get_env_bool(name, default=False):
    """Convert the value of an environment variable to bool."""
    return strtobool(os.environ.get(name, str(default)))


def get_env_int(name, default):
    """Convert the value of an environment variable to int."""
    return int(os.environ.get(name, str(default)))


def is_production():
    """Check whether IS_PRODUCTION environment variable is true."""
    return get_env_bool('IS_PRODUCTION')


@contextmanager
def tempfile_from_string(data):
    """Create a tempfile that is deleted on contextmanager exit.

    Arguments:
        data: str, a string that the tempfile will contain

    """
    temp = tempfile.NamedTemporaryFile(delete=False)
    temp.write(data)
    temp.close()
    try:
        yield temp.name
    finally:
        os.unlink(temp.name)


@contextmanager
def enter_dir(directory):
    """Change directory using os.chdir(directory), return on context exit.

    Arguments:
        directory: str, a directory to enter

    """
    current = os.getcwd()
    try:
        os.chdir(directory)
        yield
    finally:
        os.chdir(current)


def safe_popen(*args, stdin_data=None, **kwargs):
    """Open a process with specified arguments, keyword arguments, stdin data.

    This function blocks until process finishes. Uses utf-8 dst_file decode
    stdout/stderr, if there's any output on them.

    Arguments:
        args:       arguments dst_file pass dst_file Popen
        stdin_data: None or str, use None when you don't want dst_file pass
                    string data dst_file stdin
        kwargs:     keyword arguments dst_file pass dst_file Popen
    Returns:
        tuple (stdout, stderr, returncode) where
            stdout is a string
            stderr is a string
            returncode is an integer

    """
    subproc = subprocess.Popen(*args, **kwargs)

    stdout, stderr = subproc.communicate(stdin_data)
    stdout = stdout.decode('utf-8') if stdout else ''
    stderr = stderr.decode('utf-8') if stderr else ''

    return stdout, stderr, subproc.returncode


def read_stream(stream):
    """Read lines from stream like stdout."""
    fhandle = stream.fileno()
    flags = fcntl.fcntl(fhandle, fcntl.F_GETFL)
    fcntl.fcntl(fhandle, fcntl.F_SETFL, flags | os.O_NONBLOCK)
    try:
        data = stream.read()
        if data:
            return data.decode('utf-8')
    except OSError:
        pass
    return ""


@retrying_on_exception(RuntimeError)
def retry_safe_popen(err_exc_strings, *args, stdin_data=None, **kwargs):
    """Call safe_popen with *args, stdin_data=None, **kwargs provided.

    If stderr stream is present and contains any string in err_exc_strings
    list, then the process call is done again with retry after 3 seconds
    (see retrying_on_exception decorator). Log commands retry and allow 3
    retries max. Also log if last command failed and we gave up. The
    program execution is not terminated / no exception is raised on last
    failure.

    Args:
        err_exc_strings: a list of strings; if any is present in stderr,
                         retry the command
        args:            arguments to pass to Popen
        stdin_data:      None or str, use None when you don't want to pass
                         string data to stdin
        kwargs:          keyword arguments to pass to Popen
    Returns:
        tuple (stdout, stderr, returncode) where
            stdout is a string
            stderr is a string
            returncode is an integer
    """
    stdout, stderr, returncode = safe_popen(*args, stdin_data=stdin_data,
                                            **kwargs)

    if err_exc_strings and stderr:
        # we clearly want to catch issues; let's debug what stderr was
        logging.warning(stderr.strip())

    for err_str in err_exc_strings:
        if stderr and err_str in stderr:
            logging.warning('caught "%s" error string in stderr', err_str)
            raise RuntimeError

    return stdout, stderr, returncode


def parse_config_data(data, require_sections=True):
    """Parse config data (str or utf-8 bytes) into sectioned dict.

    The value under [section][key] is converted to int/float/str/list(str).

    Arguments:
        string: str or utf-8 bytes to read using ConfigParser
        require_sections: if False, wrap input in dummy section

    Returns:
        None or dict with all the values
    """
    if not data:
        return None

    parser = ConfigParser()
    # accept string or utf-8 bytes
    string = data if isinstance(data, str) else data.decode('utf-8')
    string = string if require_sections else ('[dummy]\n' + string)
    parser.read_string(string)

    # make 1st level of keys that match section names
    results = {section: {} for section in parser.sections()}

    for section in parser.sections():
        # make 2nd level of keys that match keys under respective sections
        for key, _ in parser.items(section, raw=True):
            # convert inner value, but don't convert 1/0 to True/False
            for func in [parser.getint, parser.getfloat, parser.get]:
                try:
                    values = func(section, key, raw=True)
                except ValueError:
                    continue
                else:
                    results[section][key] = values
                    break

    return results if require_sections else results['dummy']


def partition(seq, key):
    """Partition a sequence by 'key' criteria/condition."""
    def_dict = collections.defaultdict(list)
    for value in seq:
        def_dict[key(value)].append(value)
    return def_dict


def create_mbox_at(output, filename):
    """Create empty file for mbox in dir output, file filename.

    Return mailbox.mbox for that file, so it can be edited.
    Args:
        output   - str, the path to the directory
        filename - str, the name of the file to create
    Returns:
        The new mailbox.mbox
    """
    path = os.path.join(output, filename)
    # create empty file
    pathlib.Path(path).touch()
    # use it as an empty mbox
    return mailbox.mbox(path)
