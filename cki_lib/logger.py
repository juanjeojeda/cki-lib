"""Common CKI logging functions."""
import glob
import logging
import os
import subprocess
import sys
import threading

CKI_LOGS_FILEDIR = '/tmp/cki-lib.log/'
STREAM = sys.stderr
FORMAT = '%(asctime)s - [%(levelname)s] - %(name)s - %(message)s'
LOCK = threading.Lock()
CKI_HANDLER = False


def get_logger(logger_name):
    """Return CKI logger or descendant."""
    # Make sure adding handler is thread safe since get_logger might not be
    # called from main
    # https://docs.python.org/3/library/logging.html#logging.basicConfig
    with LOCK:
        global CKI_HANDLER  # pylint: disable=global-statement
        cki_handler = CKI_HANDLER
        CKI_HANDLER = True

    if not cki_handler:
        # Add STREAM handler to root logger.
        root_logger = logging.getLogger()
        handler = logging.StreamHandler(STREAM)
        root_logger.addHandler(handler)
        formatter = logging.Formatter(FORMAT)
        handler.setFormatter(formatter)

        # Set cki loglevel to CKI_LOGGING_LEVEL.
        cki_logger = logging.getLogger('cki')
        cki_logger.setLevel(os.environ.get('CKI_LOGGING_LEVEL', 'WARNING'))

    if not (logger_name == 'cki' or logger_name.startswith('cki.')):
        logger_name = 'cki.' + logger_name

    return logging.getLogger(logger_name)


def truncate_logs():
    """Truncate all .log files in the CKI_LOGS_FILEDIR directory."""
    files = glob.glob(f'{CKI_LOGS_FILEDIR}/*.log')
    # empty all logs
    subprocess.run(['truncate', '-s', '0', ] + files, check=True)


def print_logfile(filename, strict=False):
    """Read filename in CKI_LOGS_FILEDIR and print it to stdout."""
    path = os.path.join(CKI_LOGS_FILEDIR, filename)
    if os.path.isfile(path):
        print(open(path, 'r').read())
    elif strict:
        raise RuntimeError(f'{path} is not a file!')


def logger_add_fhandler(logger, dst_file, path=None):
    """Add filehandler to existing logger."""
    dirname = CKI_LOGS_FILEDIR if path is None else path
    os.makedirs(dirname, exist_ok=True)
    dst_file = os.path.join(dirname, dst_file)

    fhandler = logging.FileHandler(dst_file)
    fhandler.setLevel(logging.DEBUG)
    logger.addHandler(fhandler)


def file_logger(logger_name, dst_file='info.log', stream=STREAM,
                stream_level=os.environ.get('CKI_LOGGING_LEVEL', 'WARNING')):
    """Get CKI logger and add stream and file handlers to it.

    Output goes to dst_file in CKI_LOGS_FILEDIR
    logger_name is the name of the logger.

    Attributes:
        logger_name: str, something to identify distinct loggers
        dst_file: str, default is 'info.log', a filename to use for output
        stream_level: console output loglevel
        stream: None, sys.stderr or sys.stdout

    """
    logger = get_logger(logger_name)

    # Would probably be better to let get_logger handle propagation and
    # logging level, but can't both log DEBUG to file handler and defer to
    # root logger
    logger.propagate = False
    logger.setLevel(logging.DEBUG)

    handler = logging.StreamHandler(stream)
    logger.addHandler(handler)
    formatter = logging.Formatter(FORMAT)
    handler.setFormatter(formatter)
    handler.setLevel(stream_level)

    logger_add_fhandler(logger, dst_file)

    return logger
