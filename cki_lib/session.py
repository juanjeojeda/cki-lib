"""Helper for creating requests Session."""

import requests
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry


def get_session(user_agent, logger=None):
    """Return pre-configured requests Session.

    The session contains configured user_agent, retries
    and optional logging.
    """
    session = requests.Session()

    session.headers.update({'User-Agent': user_agent})
    if logger:
        def log_request(response, *args, **kwargs):
            # pylint: disable=unused-argument
            logger.debug('Requested: %s', response.url)

        session.hooks = {'response': log_request}

    retry = Retry(total=5, backoff_factor=1)
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)

    return session
