"""Gitlab helper."""
import datetime
import os
import subprocess
from functools import lru_cache
from urllib import parse

import gitlab
import requests
import yaml
from dateutil.parser import parse as date_parse

from cki_lib.logger import get_logger
from cki_lib.misc import safe_popen
from cki_lib.session import get_session

GITLAB_TIMESTAMP_FORMAT = "%Y-%m-%dT%H:%M:%S.%fZ"

LOGGER = get_logger(__name__)
SESSION = get_session(__name__, LOGGER)


def get_token(url, env_name=None):
    """Return a Gitlab API token.

    The tokens have to be provided as environment variables like this:
        export GITLAB_TOKENS='{"gitlab.com": "COM_GITLAB_TOKEN"}'
        export COM_GITLAB_TOKEN='1234567890abcedf'

    Parameters:
        url: GitLab instance URL
        env_name: name of the environment variable with a JSON host=name dict
            containing the names of the environment variables with the tokens
    """
    host = parse.urlsplit(url).hostname
    # would be nice to get tokens from a '.config/cki/config'/CKI_CONFIG file
    token_names = yaml.safe_load(
        os.environ.get(env_name or 'GITLAB_TOKENS', '{}'))
    token_name = token_names.get(host)
    return os.environ.get(token_name) if token_name else None


def get_instance(url, token=None, env_name=None):
    """Return a Gitlab API instance.

    Parameters:
        url: GitLab instance URL
        token: private GitLab API token
        env_name: environment variable name to use for get_token()
    """
    return gitlab.Gitlab(url, session=SESSION,
                         private_token=token or get_token(url, env_name))


class GitlabHelper:
    """Helper for Gitlab requests."""

    def __init__(self, url, token):
        """Initialize the helper."""
        self.url = url
        self.token = token
        self.api = gitlab.Gitlab(url, token)
        self.project = None

        self.requests_per_page = 10
        self.requests_max_iter = 50

    def set_project(self, project_id):
        """Initialize project to work on."""
        self.project = self._get_project(project_id)

    @lru_cache(maxsize=None)
    def _get_project(self, project_id):
        """Return a project instance from project_id. With cache."""
        return self.api.projects.get(project_id)

    def get_raw_jobs(self, pipeline_id, exclude_not_run=True):
        """Get "raw jobs" (attributes) of a given pipeline_id.

        Args:
            pipeline_id: int, an id of the pipeline for which we get the jobs
            exclude_not_run: bool, when true, include only jobs with
                             ['success', 'failed', 'manual'] status
        Returns:
            a list of attributes for each pipeline
        """
        pipes = self.project.pipelines.get(pipeline_id)

        results = []
        for job in pipes.jobs.list(all=True):
            attributes = job.attributes
            if not exclude_not_run or attributes['status'] in ['success',
                                                               'failed',
                                                               'manual']:
                results.append(attributes)

        return results

    def get_artifact(self, job_id, name, raw=False):
        """
        Download artifact from self.project.

        job_id:     Gitlab's job id from where to get the artifacts.
        name:       Filename of the file to download.
        """
        url = f"{self.url}/api/v4/projects/" \
              f"{self.project.id}/jobs/{job_id}/artifacts/{name}"

        headers = {'Private-Token': self.token}
        resp = requests.get(url, headers=headers)

        # Handle any fatal errors like 404's.
        if resp.status_code != requests.codes.ok:  # pylint: disable=no-member
            return None

        # If raw=True, don't try to encode into json.
        if raw:
            return resp.content

        # Try to return JSON, otherwise return text.
        try:
            return resp.json()
        except ValueError:
            return resp.content

    def get_pipelines_since(self, since):
        """
        Get pipelines newer than "since" date.

        since: timezone-aware or naive datetime object
        """
        pipes = []

        for page_counter in range(1, self.requests_max_iter + 1):
            pipes.extend(
                self.project.pipelines.list(
                    per_page=self.requests_per_page,
                    page=page_counter)
            )

            # Checking date for the last element only.
            # Not really precise but we save a lot of requests :)
            if not pipes:
                return []
            pipe = self.project.pipelines.get(pipes[-1].id)

            if since.tzinfo is None:
                pipe_created_at = datetime.datetime.strptime(
                    pipe.created_at, GITLAB_TIMESTAMP_FORMAT)

            else:
                pipe_created_at = date_parse(pipe.created_at)

            if pipe_created_at < since:
                break

        return pipes

    def clone_project(self, dst_dir=None, protocol='ssh'):
        """Clone the project using 'git' to dst_dir."""
        url = self.project.ssh_url_to_repo if protocol == 'ssh' else \
            self.project.http_url_to_repo
        args = ['git', 'clone', url]
        if dst_dir:
            args.append(dst_dir)

        stdout, stderr, retcode = safe_popen(args, stdout=subprocess.PIPE,
                                             stderr=subprocess.PIPE)
        if retcode:
            LOGGER.warning(stdout)
            LOGGER.error(stderr)
            raise RuntimeError('cloning project failed')
