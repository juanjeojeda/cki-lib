#!/bin/bash
set -euo pipefail

# pass the packages to test (without tests directory) as args to the script

function say { echo "$@" | toilet -f mono12 -w 300 | lolcat -f || echo "$@"; }
function echo_green { echo -e "\e[1;32m${1}\e[0m"; }
function echo_red { echo -e "\e[1;31m${1}\e[0m"; }
function echo_yellow { echo -e "\e[1;33m${1}\e[0m"; }

say "preparing"

PACKAGES=(flake8 pydocstyle pylint pytest coverage)

if [[ "$(type -P python3)" = /usr* ]]; then
    python3 -m pip install --user "${PACKAGES[@]}"
else
    python3 -m pip install "${PACKAGES[@]}"
fi

say "linting"

declare -i FAILED=0

# Check ALL Python files with flake8 (which includes pycodestyle) and pydocstyle

echo_yellow "Running flake8"
if ! flake8 --exclude ".*" .; then
    FAILED+=1
fi

echo_yellow "Running pydocstyle"
if ! pydocstyle; then
    FAILED+=1
fi

# Only run pylint and coverage for the specified packages

echo_yellow "Running pylint"
if ! pylint "$@"; then
    FAILED+=1
fi

echo_yellow "Running pytests under coverage"
if ! coverage run --source "$(IFS=,; echo "$*")" --branch -m pytest --ignore inttests/ --color=yes -v -r s; then
    FAILED+=1
fi

echo_yellow "Generating coverage reports"
coverage report -m || true
coverage html -d coverage/ || true

if [ "${FAILED}" -gt 0 ]; then
    echo_red "$FAILED linting steps failed."
    exit 1
fi

